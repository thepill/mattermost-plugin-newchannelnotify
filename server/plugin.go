package main

import (
	"fmt"
	"strings"
	"sync"

	"github.com/mattermost/mattermost/server/public/model"
	"github.com/mattermost/mattermost/server/public/plugin"
)

const defaultBotName = "newchannelbot"

type NewChannelNotifyPlugin struct {
	plugin.MattermostPlugin

	// configurationLock synchronizes access to the configuration.
	configurationLock sync.RWMutex

	// configuration is the active plugin configuration. Consult getConfiguration and
	// setConfiguration for usage.
	configuration *configuration
}

func (p *NewChannelNotifyPlugin) OnActivate() error {
	p.API.LogInfo("Plugin loaded")
	return nil
}

// https://play.golang.org/p/Qg_uv_inCek
// contains checks if a string is present in a slice
func containsCaseInsensitive(s []string, str string) bool {
	for _, v := range s {
		if strings.EqualFold(v, str) {
			return true
		}
	}

	return false
}

func (p *NewChannelNotifyPlugin) ChannelHasBeenCreated(c *plugin.Context, channel *model.Channel) {
	log := fmt.Sprintf("ChannelHasBeenCreated for channel with id [%s], type [%s] triggered", channel.Id, channel.Type)
	p.API.LogDebug(log)

	config := p.getConfiguration()

	// Check if only specific teams are being watched and notified
	if config.TeamsToWatch != "" {
		team, err := p.API.GetTeam(channel.TeamId)
		if err != nil {
			p.API.LogError(err.Message)
		}
		p.API.LogDebug(fmt.Sprintf("team: %s", team.Name))

		teamsToWatch := strings.Split(config.TeamsToWatch, ";")

		if !containsCaseInsensitive(teamsToWatch, team.Name) {
			p.API.LogDebug(fmt.Sprintf("team %s is not watched - skipping", team.Name))
			return
		}
	}

	if config.BotUserName == "" {
		config.BotUserName = defaultBotName
	}

	if config.ChannelToPost == "" {
		config.ChannelToPost = model.DefaultChannelName
	}

	newChannelName := channel.Name
	channelPurpose := channel.Purpose

	if channel.Type == model.ChannelTypeDirect || channel.Type == model.ChannelTypeGroup {
		return
	}

	if channel.Type == model.ChannelTypePrivate {
		if !config.IncludePrivateChannels {
			return
		}
		newChannelName += " [Private]"
	}

	p.ensureBotExists()
	bot, err := p.API.GetUserByUsername(config.BotUserName)
	if err != nil {
		p.API.LogError(err.Message)
		return
	}

	mainChannel, err := p.API.GetChannelByName(channel.TeamId, config.ChannelToPost, false)
	if err != nil {
		p.API.LogError(err.Message)
		return
	}

	createdBy := ""
	if channel.CreatorId != "" {
		// Disable lint because of error 'shadow: declaration of "err" shadows declaration at line' - dont know why...
		//nolint:govet
		creator, err := p.API.GetUser(channel.CreatorId)
		if err != nil {
			p.API.LogError(err.Message)
			return
		}

		createdBy = creator.Username
	}

	message := ConvertMessage(config.MessageTemplate, createdBy, newChannelName, channelPurpose)

	post, err := p.API.CreatePost(&model.Post{
		ChannelId: mainChannel.Id,
		UserId:    bot.Id,
		Message:   config.Mention + message,
	})

	if err != nil {
		p.API.LogError(err.Message)
		return
	}

	p.API.LogDebug(fmt.Sprintf("Created post %s", post.Id))
}

func ConvertMessage(messageTemplate string, channelCreator string, channelName string, channelPurpose string) string {
	if messageTemplate == "" {
		messageTemplate = "Hello there :wave:. You might want to check out the new channel {channel::nameAsLink} created by {channel::creator}. {channel::name}'s purpose is '{channel::purpose}'"
	}

	message := messageTemplate

	message = strings.ReplaceAll(message, "{channel::creator}", " @"+channelCreator)
	message = strings.ReplaceAll(message, "{channel::nameAsLink}", "~"+channelName)
	message = strings.ReplaceAll(message, "{channel::name}", channelName)
	if channelPurpose != "" {
		channelPurpose = "*" + channelPurpose + "*"
	}
	message = strings.ReplaceAll(message, "{channel::purpose}", channelPurpose)

	return message
}
