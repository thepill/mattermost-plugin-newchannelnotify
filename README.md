# Mattermost New Channel Notify Plugin

A plugin for Mattermost to notify all users about newly created channels.

![screenshot](https://i.imgur.com/JzGxNk8.png)

## Installation

**Requires Mattermost 6.4 or higher.**

1. Download the [latest release here](https://gitlab.com/-/project/11871381/uploads/f8393596f425a30a93493a122e13ff18/mattermost-plugin-newchannelnotify-2.0.0.tar.gz) (SHA256: `e5511cca3fc9c4425ee3cb8551936c072edc783c598e59059fbffbe19360e97e`)

2. Upload the plugin via the [System Console](https://about.mattermost.com/default-plugin-uploads).

3. Enable the plugin via the System Console

4. Optionally, change `settings` under the plugins settings menu in System Console:

-   Bot Name
-   Channel to Postw
-   Message Template
-   Include private channels
-   Mention (see Note below)
-   TeamsToWatch

**A note about Mentions**:
Mentions (e.g @channel) will only work properly if

-   the plugin has run once so the bot account is created (create a new public-test channel)
-   the bot account has than been assigned to the team
-   the bot account has been added to the channel where it will post (if not default `town-square`)

## Developing

See https://github.com/mattermost/mattermost-plugin-starter-template#development

Use `make dist` to build distributions of the plugin that you can upload to a Mattermost server.

Use `make deploy` to deploy the plugin to your local server. Before running `make deploy` you need to set a few environment variables:

```
export MM_SERVICESETTINGS_SITEURL=http://localhost:8065
export MM_ADMIN_USERNAME=admin
export MM_ADMIN_PASSWORD=password
```
